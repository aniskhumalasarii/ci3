<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserModel extends CI_Model {

    public function get_data($table) 
    {
        return $this->db->get($table);
    }

    public function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    // update user
    public function update_data($data, $table)
    {
        $this->db->where('id', $data['id']);
        $this->db->update($table, $data);
    }

    public function delete($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    
    // update barang
    public function update_barang($data, $table)
    {
        $this->db->where('id_barang', $data['id_barang']);
        $this->db->update($table, $data);
    }

    public function getdata()
    {
        $barang = $this->db->query("SELECT * FROM kategori k JOIN barang b ON k.id_kategori = b.id_kategori ORDER BY k.id_kategori DESC");

        return $barang->result();
    }

    public function getkategori()
    {
        $datakategori = $this->db->query("SELECT * FROM kategori ORDER BY nama_kategori");

        return $datakategori->result();
    }

    public function save($data)
    {
        $this->db->insert('barang',$data);

        return true;
    }
    // public getdata()
    // {
    //    $this->db->select('*');
    //    $this->db->from('kategori k');
    //    $this->db->join('barang b', 'k.id_kategori = b.id_kategori');
    //    $this->db-order_by('k.id_kategori', 'desc');

    //    return $this->db->get()->result();
    // }

    // update kategori
    public function update_kategori($data, $table)
    {
        $this->db->where('id_kategori', $data['id_kategori']);
        $this->db->update($table, $data);
    }

    // join tablel barang dan kategori
    public function join($table, $tbjoin, $join)
    {
        $this->db->join($tbjoin, $join);
        return $this->db->get($table);
    }

    //dashboard
    public function jumlah_barang()
    {
        $this->db->select('*');
        $this->db->from('barang');
        return $this->db->get()->num_rows();
    }

    public function kategori_barang()
    {
        $this->db->select('*');
        $this->db->from('kategori');
        return $this->db->get()->num_rows();
    }

    public function jumlah_user()
    {
        $this->db->select('*');
        $this->db->from('user');
        return $this->db->get()->num_rows();
    }

    public function get_keyword($keyword) {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->like('name', $keyword);
        $this->db->or_like('email', $keyword);
        $this->db->or_like('username', $keyword);
        $this->db->or_like('level', $keyword);
        return $this->db->get()->result();
    }

    public function get_keyword_barang($keyword) {
        $this->db->select('*');
        $this->db->from('barang');
        $this->db->like('nama_barang', $keyword);
        $this->db->like('id_kategori', $keyword);
        $this->db->or_like('jumlah', $keyword);
        $this->db->or_like('merek', $keyword);
        $this->db->or_like('jenis', $keyword);
        $this->db->or_like('harga', $keyword);
        $this->db->or_like('satuan', $keyword);
        return $this->db->get()->result();
    }

    public function get_keyword_kategori($keyword) {
        $this->db->select('*');
        $this->db->from('kategori');
        $this->db->like('nama_kategori', $keyword);
        return $this->db->get()->result();
    }

}