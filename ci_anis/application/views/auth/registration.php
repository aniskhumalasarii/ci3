
	<div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('assets/images/bg-01.jpeg');"></div>
			
			<div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
				<form class="login100-form validate-form" method="post" action="<?= base_url('registration'); ?>" >
				    <span class="login100-form-title p-b-59">
						Sign Up
					</span>
                    
					<div class="wrap-input100 validate-input" data-validate="Name is required">
						<span class="label-input100">Full Name</span>
						<input class="input100" type="text" id="name" name="name" placeholder="Name" value="<?= set_value('name'); ?>">
						<span class="focus-input100"></span>
						<?= form_error('name', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<span class="label-input100">Email</span>
						<input class="input100" type="text" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
						<span class="focus-input100"></span>
						<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Username is required">
						<span class="label-input100">Username</span>
						<input class="input100" type="text" id="username" name="username" placeholder="Username" value="<?= set_value('username'); ?>">
						<?= form_error('username', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password1" name="password1" placeholder="password"  value="<?= set_value('password1'); ?>">
						<?= form_error('password1', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>
					
					<div class="wrap-input100 validate-input" data-validate = "Repeat Password is required">
						<span class="label-input100">Repeat Password</span>
						<input class="input100" type="password" id="password2" name="password2" placeholder="Repeat Password"  value="<?= set_value('password2'); ?>">
						<span class="focus-input100"></span>
						<?= form_error('password2', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="flex-m w-full p-b-33">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								<span class="txt1">
									I agree to the
									<a href="#" class="txt2 hov1">
										Terms of User
									</a>
								</span>
							</label>
						</div>
                    </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn">
								Sign Up
							</button>
						</div>

						<a href="<?= base_url('Auth'); ?>" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
							Sign in
							<i class="fa fa-long-arrow-right m-l-5"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
