	
  <div class="limiter">
		<div class="container-login100">
			<div class="login100-more" style="background-image: url('assets/images/bg-01.jpeg');"></div>

			<div class="wrap-login100 p-l-50 p-r-50 p-t-210 p-b-50">

				<form class="login100-form validate-form" method="post" action="<?= base_url('auth'); ?>">
					<span class="login100-form-title p-b-59">
						Sign in
					</span>

					<?= $this->session->flashdata('messagge'); ?>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<span class="label-input100">Email</span>
						<input class="input100" type="text" id="email" name="email" placeholder="Email" value="<?= set_value('email'); ?>">
						<span class="focus-input100"></span>
						<?= form_error('email', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<span class="label-input100">Password</span>
						<input class="input100" type="password" id="password" name="password" placeholder="Password" value="<?= set_value('password'); ?>">
						<span class="focus-input100"></span>
						<?= form_error('password', '<small class="text-danger pl-3">', '</small>'); ?>
					</div>

                    <div class="flex-m w-full p-b-33">
						<span class="txt1">
							Forgot password?
							<a href="#" class="txt2 hov1">
								click here
							</a>
						</span>
                    </div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="sumbit">
								Sign in
							</button>
						</div>


						<a href="<?= base_url('registration'); ?>" class="dis-block txt3 hov1 p-r-30 p-t-10 p-b-10 p-l-30">
							Sign Up
							<i class="fa fa-long-arrow-right m-l-5"></i>
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	
  