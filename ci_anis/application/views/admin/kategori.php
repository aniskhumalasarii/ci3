<?= $this->session->flashdata('message'); ?>

<div class="container-fluid">                   
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title"> Daftar Kategori</h3>
                    <p><buton data-toggle="modal" data-target="#add" class="btn btn-primary"><i class="fas fa-plus"></i> Add Kategori</buton></p>
                    <div class="row">
                        <div class="col-md-4 ms-auto">
                            <?php echo form_open('admin/search_k'); ?>
                                <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search..." name="keyword" autocomplete="off" autofocus>
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">Search</button>
                                    </div>
                                </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th class="border-top-0">No</th>
                                    <th class="border-top-0">Nama Kategori</th>
                                    <th class="border-top-0">Action</th>
                                </tr>
                            </thead>
                            <?php $no = 1;
                            foreach ($kategori as $k) : ?>
                            <tbody>
                                <tr class="text-center">
                                    <td><?= $no++ ?></td>
                                    <td><?= $k->nama_kategori ?></td>
                                    <td>
                                        <button data-toggle="modal" data-target="#edit<?= $k->id_kategori?>" class="btn btn-warning btn"><i class="fas fa-edit"></i></button>
                                        <a href="<?= base_url('admin/delete_kategori/' . $k->id_kategori) ?>" class="btn btn-danger"><i class="fas fa-trash" onclick="return confirm('Are you sure want to delete this data?')"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php endforeach ?>
                        </table>
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <?php foreach ($kategori as $k) : ?>
    <div class="modal fade" id="add" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"> Add Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/add/' . $k->id_kategori); ?>" method="POST">
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Nama Kategori</label>
                        <input type="text" name="nama_kategori" class="form-control" value="<?= set_value('nama_kategori'); ?>" required>
                    </div>
                    <div class="modal-footer mt-4">
                        <button type="sumbit" class="btn btn-primary btn mt-3"> Save</button>
                        <a href="<?= base_url('admin/kategori');?>" class="btn btn-danger btn mt-3"> Cancel</a>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    <?php endforeach ?> 

    <!-- Modal Edit -->
    <?php foreach ($kategori as $k) : ?>
    <div class="modal fade" id="edit<?= $k->id_kategori ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/edit_kategori/' . $k->id_kategori); ?>" method="POST">
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Nama Kategori</label>
                        <input type="text" name="nama_kategori" class="form-control" value="<?= $k->nama_kategori ?>" required>
                        <?= form_error('name', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="modal-footer mt-4">
                        <button type="sumbit" class="btn btn-primary mt-3"> Save</button>
                        <button type="reset" class="btn btn-danger mt-3"> Reset</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    <?php endforeach ?> 
</div>