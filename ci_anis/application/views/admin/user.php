<?= $this->session->flashdata('message'); ?>

<div class="container-fluid">                   
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">Daftar User</h3>
                <p><a href="<?= base_url('user/add');?>" class="btn btn-primary"><i class="fas fa-plus"></i> Add User</a></p>
                <div class="row">
                    <div class="col-md-4 ms-auto">
                        <?php echo form_open('user/search'); ?>
                            <div class="input-group mb-3">
                            <input type="text" class="form-control" placeholder="Search..." name="keyword" autocomplete="off" autofocus>
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">Search</button>
                                </div>
                            </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th class="border-top-0">No</th>
                                    <th class="border-top-0">Full Name</th>
                                    <th class="border-top-0">Email</th>
                                    <th class="border-top-0">Username</th>
                                    <th class="border-top-0">Level</th>
                                    <th class="border-top-0">Action</th>
                                </tr>
                            </thead>
                            <?php $no = 1;
                            foreach ($user as $usr) : ?>
                            <tbody>
                                <tr class="text-center">
                                    <td><?= $no++ ?></td>
                                    <td><?= $usr->name ?></td>
                                    <td><?= $usr->email ?></td>
                                    <td><?= $usr->username ?></td>
                                    <td><?= $usr->level ?></td>
                                    <td>
                                        <button data-toggle="modal" data-target="#edit<?= $usr->id ?>" class="btn btn-warning btn"><i class="fas fa-edit"></i></button>
                                        <a href="<?= base_url('user/delete/' . $usr->id) ?>" class="btn btn-danger"><i class="fas fa-trash" onclick="return confirm('Are you sure want to delete this data?')"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php endforeach ?>
                        </table>
                    </div>
            </div>
        </div> 
    </div>

    <!-- Modal Edit -->
    <?php foreach ($user as $usr) : ?>
    <div class="modal fade" id="edit<?= $usr->id ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">User edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <form action="<?= base_url('user/edit/' . $usr->id); ?>" method="POST">
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Full Name</label>
                        <input type="text" name="name" class="form-control" value="<?= $usr->name ?>" required>
                        <?= form_error('name', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Email</label>
                        <input type="text" name="email" class="form-control" value="<?= $usr->email ?>">
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Username</label>
                        <input type="text" name="username" class="form-control" value="<?= $usr->username ?>" required>
                        <?= form_error('username', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Level</label>
						<div class="input-group mb-3">
							<select class="level" name="level" id="level">
								<option selected value='0' disabled="">Choose...</option>
                                <option value="admin">Admin</option>
								<option value="member">Member</option>
							</select>
						</div>
						<?= form_error('level', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Image</label>
                        <input type="file" name="userfile" class="form-control" size="20">
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Password</label>
                        <input type="password" name="password" class="form-control" id="password">
                    </div>
                    <div class="modal-footer mt-4">
                        <button type="sumbit" class="btn btn-primary mt-3"> Save</button>
                        <button type="reset" class="btn btn-danger mt-3"> Reset</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
    <?php endforeach ?> 
</div>