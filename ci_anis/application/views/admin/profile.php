<div class="container-fluid">
    <div class="row">
        <div class="col-lg-4 col-xlg-3 col-md-12">
            <div class="white-box">
                <div class="user-bg"> <img width="100%" alt="user" src="<?= base_url('assets/plugins/images/large/bg-profile.jpeg');?>">
                    <div class="overlay-box">
                        <div class="user-content">
                            <a href="javascript:void(0)"><img src="<?= base_url('assets/images/profile/') . $input['image']; ?>"
                                    class="thumb-lg img-circle" alt="img"></a>
                            <h4 class="text-white mt-2"><?= $input['username']; ?></h4>
                            <h5 class="text-white mt-2"><?= $input['email'] ?></h5>
                        </div>
                    </div>
                </div>
                <div class="user-btm-box mt-5 d-md-flex">
                    <div class="col-md-4 col-sm-4 text-center">
                        <h1>258</h1>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <h1>125</h1>
                    </div>
                    <div class="col-md-4 col-sm-4 text-center">
                        <h1>556</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-xlg-9 col-md-12">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal form-material">
                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Full Name</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="name"
                                    class="form-control p-0 border-0" value="<?= $input['name'] ?>" readonly></div>
                        </div>
                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Username</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="text" name="username"
                                    class="form-control p-0 border-0" value="<?= $input['username'] ?>" readonly></div>
                        </div>
                        <div class="form-group mb-4">
                            <label for="example-email" class="col-md-12 p-0">Email</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="email"
                                    class="form-control p-0 border-0" name="example-email"
                                    id="example-email" value="<?= $input['email'] ?>" readonly>
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Password</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="password" value="password" readonly class="form-control p-0 border-0">
                            </div>
                        </div>
                        <div class="form-group mb-4">
                            <label class="col-md-12 p-0">Level</label>
                            <div class="col-md-12 border-bottom p-0">
                                <input type="level" value="<?= $input['level'] ?>" readonly class="form-control p-0 border-0">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
