
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-lg-4 col-md-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Jumlah Barang</h3>
                <ul class="list-inline two-part d-flex align-items-center mb-0">
                    <li>
                        <div id="sparklinedash"><canvas width="67" height="30"
                                style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas>
                        </div>
                    </li>
                    <li class="ms-auto"><span class="counter text-success"><?= $jumlah ?></span></li>
                </ul>
                <div class="dark-box text-right mt-4">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url('admin/barang');?>"
                        aria-expanded="false">
                        <span class="hide-menu">More detail</span>
                        <i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Jumlah Kategori</h3>
                <ul class="list-inline two-part d-flex align-items-center mb-0">
                    <li>
                        <div id="sparklinedash2"><canvas width="67" height="30"
                                style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas>
                        </div>
                    </li>
                    <li class="ms-auto"><span class="counter text-purple"><?= $kategori ?></span></li>
                </ul>
                <div class="dark-box text-right mt-4">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url('admin/kategori');?>"
                        aria-expanded="false">
                        <span class="hide-menu">More detail</span>
                        <i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-12">
            <div class="white-box analytics-info">
                <h3 class="box-title">Jumlah User</h3>
                <ul class="list-inline two-part d-flex align-items-center mb-0">
                    <li>
                        <div id="sparklinedash4"><canvas width="67" height="30"
                                style="display: inline-block; width: 67px; height: 30px; vertical-align: top;"></canvas>
                        </div>
                    </li>
                    <li class="ms-auto"><span class="counter text-info"><?= $user ?></span>
                    </li>
                </ul>
                <div class="dark-box text-right mt-4">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?= base_url('user');?>"
                        aria-expanded="false">
                        <span class="hide-menu">More detail</span>
                        <i class="fas fa-arrow-circle-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
