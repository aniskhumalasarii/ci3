<?= $this->session->flashdata('message'); ?>

<div class="container-fluid">                   
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <h3 class="box-title">Daftar Barang</h3>
                    <p><buton data-toggle="modal" data-target="#add_action" class="btn btn-primary"><i class="fas fa-plus"></i> Add Barang</buton></p>
                    <div class="row">
                        <div class="col-md-4 ms-auto">
                            <?php echo form_open('admin/search'); ?>
                                <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="Search..." name="keyword" autocomplete="off" autofocus>
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="submit">Search</button>
                                    </div>
                                </div>
                            <?php echo form_close() ?>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered table-striped" style="width:100%">
                            <thead>
                                <tr class="text-center">
                                    <th class="border-top-0">No</th>
                                    <th class="border-top-0">Nama Barang</th>
                                    <th class="border-top-0">Kategori</th>
                                    <th class="border-top-0">Jumlah</th>
                                    <th class="border-top-0">Merek</th>
                                    <th class="border-top-0">Jenis</th>
                                    <th class="border-top-0">Harga (Rp)</th>
                                    <th class="border-top-0">Satuan</th>
                                    <th class="border-top-0">Action</th>
                                </tr>
                            </thead>
                            <?php $no = 1;
                            foreach ($barang as $b) : ?>
                            <tbody>
                                <tr class="text-center">
                                    <td><?= $no++ ?></td>
                                    <td><?= $b->nama_barang ?></td>
                                    <td><?= $b->nama_kategori ?></td>
                                    <td><?= $b->jumlah ?></td>
                                    <td><?= $b->merek ?></td>
                                    <td><?= $b->jenis ?></td>
                                    <td><?= $b->harga ?></td>
                                    <td><?= $b->satuan ?></td>
                                    <td>
                                        <button data-toggle="modal" data-target="#edit<?= $b->id_barang ?>" class="btn btn-warning btn"><i class="fas fa-edit"></i></button>
                                        <a href="<?= base_url('admin/delete/' . $b->id_barang) ?>" class="btn btn-danger"><i class="fas fa-trash" onclick="return confirm('Are you sure want to delete this data?')"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                            <?php endforeach ?>
                        </table>
                    </div>
            </div>
        </div>
    </div>

    <!-- Modal Add -->
    <?php foreach ($barang as $b) : ?>
    <div class="modal fade" id="add_action" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            <?= form_open_multipart('admin/add_action/' . $b->id_barang); ?>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" value="<?= set_value('nama_barang'); ?>" required>
                    </div>
                    <!--<div class="form group col-md-12 mb-3 mt-3">
                        <label>Kategori</label>
                        <input type="text" name="id_kategori" class="form-control" value="<?= set_value('id_kategori'); ?>" required>
                    </div>-->
                    <div class="form group col-md-12 mb-6 mt-3">
                        <label>kategori</label>
                        <div class="input-group">
							<select name="id_kategori" id="id_kategori">
								<option selected value="" disabled="">Choose...</option>
								<?php foreach ($datakategori as $dk): ?>
                                <option value="<?= $dk->id_kategori ?>"><?= $dk->nama_kategori ?></option>
                                <?php endforeach ?>
							</select>
						</div>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" class="form-control" value="<?= set_value('jumlah'); ?>" required>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Merek</label>
                        <input type="text" name="merek" class="form-control" value="<?= set_value('merek'); ?>" required>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Jenis</label>
                        <input type="text" name="jenis" class="form-control" value="<?= set_value('jenis'); ?>" required>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control" value="<?= set_value('harga'); ?>" required>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Satuan</label>
                        <input type="text" name="satuan" class="form-control" value="<?= set_value('satuan'); ?>" required>
                    </div>
                    <div class="modal-footer mt-4">
                    <button type="sumbit" class="btn btn-primary btn mt-3">Save</button>
                    <a href="<?= base_url('admin/barang');?>" class="btn btn-danger btn mt-3"> Cancel</a>
                    </div>
                <?= form_close(); ?>
            </div>
            </div>
        </div>
    </div>
    <?php endforeach ?>


    <!-- Modal Edit -->
    <?php foreach ($barang as $b) : ?>
    <div class="modal fade" id="edit<?= $b->id_barang ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
                <form action="<?= base_url('admin/edit/' . $b->id_barang); ?>" method="POST">
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" value="<?= $b->nama_barang ?>" required>
                        <?= form_error('name', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-6 mt-3">
                        <label>kategori</label>
                        <div class="input-group">
                            <select name="id_kategori" id="id_kategori">
                                <option selected value="" disabled="">Choose...</option>
                                <?php foreach ($datakategori as $dk): ?>
                                <option value="<?= $dk->id_kategori ?>"><?= $dk->nama_kategori ?></option>
                                <?php endforeach ?>
							</select>
						</div> 
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Jumlah</label>
                        <input type="text" name="jumlah" class="form-control" value="<?= $b->jumlah ?>">
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Merek</label>
                        <input type="text" name="merek" class="form-control" value="<?= $b->merek ?>" required>
                        <?= form_error('merek', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Jenis</label>
                        <input type="text" name="jenis" class="form-control" value="<?= $b->jenis ?>">
                        <?= form_error('jenis', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Harga</label>
                        <input type="text" name="harga" class="form-control" value="<?= $b->harga ?>">
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Satuan</label>
                        <input type="text" name="satuan" class="form-control" value="<?= $b->satuan ?>">
                    </div>
                    <div class="modal-footer mt-4">
                        <button type="sumbit" class="btn btn-primary mt-3"> Save</button>
                        <button type="reset" class="btn btn-danger mt-3"> Reset</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
<?php endforeach ?>
</div>