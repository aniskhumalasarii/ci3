<!DOCTYPE html>
<html lang="en">
<head>
	<title>Add</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?= base_url('assets/images-add/icons/favicon.ico')?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/bootstrap/css/bootstrap.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts-add/font-awesome-4.7.0/css/font-awesome.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/fonts-add/Linearicons-Free-v1.0.0/icon-font.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/animate/animate.css')?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/css-hamburgers/hamburgers.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/animsition/css/animsition.min.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/select2/select2.min.css')?>">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/vendor-add/daterangepicker/daterangepicker.css')?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css-add/util.css')?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css-add/main.css')?>">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-100 p-r-100 p-t-40 p-b-40">
				<form action="<?= base_url('user/add_action'); ?>" method="POST" class="login100-form validate-form flex-sb flex-w">
					<span class="login100-form-title p-b-32">
						Add User
					</span>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Full Name</label>
                        <input type="text" name="name" class="form-control" value="<?= set_value('name'); ?>">
                        <?= form_error('name', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Email</label>
                        <input type="text" name="email" class="form-control" value="<?= set_value('email'); ?>">
                        <?= form_error('email', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Username</label>
                        <input type="text" name="username" class="form-control" value="<?= set_value('username'); ?>">
                        <?= form_error('username', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label> Level</label>
						<div class="input-group mb-3">
							<select class="level" name="level" id="level">
								<option selected value='0' disabled="">Choose...</option>
								<option value="admin">Admin</option>
								<option value="member">Member</option>
							</select>
						</div>
						<?= form_error('level', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Image</label>
                        <input type="file" name="userfile" class="form-control" id="userfile" size="20">
                    </div>
                    <div class="form group col-md-12 mb-3 mt-3">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value="<?= set_value('password'); ?>">
                        <?= form_error('password', '<div class="text-small text-danger">', '</div>'); ?>
                    </div>
                    <button type="sumbit" class="btn btn-primary btn mt-3">Save</button>
                    <a href="<?= base_url('user');?>" class="btn btn-danger btn mt-3"> Cancel</a>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/jquery/jquery-3.2.1.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/animsition/js/animsition.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/bootstrap/js/popper.js')?>"></script>
	<script src="<?= base_url('vendor-add/bootstrap/js/bootstrap.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/select2/select2.min.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/daterangepicker/moment.min.js')?>"></script>
	<script src="<?= base_url('assets/vendor-add/daterangepicker/daterangepicker.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/vendor-add/countdowntime/countdowntime.js')?>"></script>
<!--===============================================================================================-->
	<script src="<?= base_url('assets/js-add/main.js')?>"></script>

</body>
</html> 

