<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller 
{

	public function __construct(){
		parent::__construct();

		if (!$this->session->userdata('email')) {
			redirect('auth');
		}
		$this->load->model('UserModel');
	}

	public function index()
	{
		$data['title'] = 'Rain Store || Dashboard';
		$data['sidebar'] = 'Dashboard';
		$data['jumlah'] = $this->UserModel->jumlah_barang();
		$data['kategori'] = $this->UserModel->kategori_barang();
		$data['user'] = $this->UserModel->jumlah_user();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/dashboard', $data);
		$this->load->view('templates/admin_footer');
	}
	

	// Profile
	public function profile()
	{
		$data['title'] = 'Rain Store || Profile';
		$data['sidebar'] = 'Profile';
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/profile', $data);
		$this->load->view('templates/admin_footer');
	}


	// Kategori
	public function kategori()
	{
		$data['title'] = 'Rain Store || Kategori';
		$data['sidebar'] = 'Kategori Barang';
		$data['kategori'] = $this->UserModel->get_data('kategori')->result();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/kategori', $data);
		$this->load->view('templates/admin_footer');
	}

	public function add($id_kategori)
	{
		$data = array (
			'nama_kategori'=> $this->input->post('nama_kategori'),
		);

		$this->UserModel->insert_data($data, 'kategori');
		$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Data added successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/kategori');
    }

	public function edit_kategori($id_kategori)
	{
		$data = array (
			'id_kategori' => $id_kategori,
			'nama_kategori'=> $this->input->post('nama_kategori'),
		);

		$this->UserModel->update_kategori($data, 'kategori');
		$this->session->set_flashdata('message', '<div class="alert alert-primary alert-dismissible fade show" role="alert">Data successfully changed!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/kategori');
		
	}

	public function delete_kategori($id_kategori)
	{
		$where = array('id_kategori' => $id_kategori);

		$this->UserModel->delete($where, 'kategori');
		$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Data deleted successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/kategori');
	}

	// Barang
	public function barang()
	{
		$data['title'] = 'Rain Store || Barang';
		$data['sidebar'] = 'Daftar Barang';
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
		$data ['barang'] = $this->UserModel->getdata();
		$data['datakategori'] = $this->UserModel->getkategori();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/barang', $data);
		$this->load->view('templates/admin_footer');
	}	

	public function add_action($id_barang)
	{
		$data = array (
			'nama_barang'=> $this->input->post('nama_barang'),
			'id_kategori'=> $this->input->post('id_kategori'),
			'jumlah'=> $this->input->post('jumlah'),
			'merek'=> $this->input->post('merek'),
			'jenis'=> $this->input->post('jenis'),
			'harga' => $this->input->post('harga'),
			'satuan' =>  $this->input->post('satuan')
		);

		$save = $this->UserModel->save($data);

		if ($save) {
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Data added successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/barang', 'refresh');
		} else {
			$this->load->view('admin/barang', 'refresh');
		}
		
	}

	// // Barang
	// public function barang()
	// {
	// 	$data['title'] = 'Rain Store || Barang';
	// 	$data['sidebar'] = 'Daftar Barang';
	// 	$data['barang'] = $this->UserModel->join('barang', 'kategori', 'barang.id_kategori=kategori.id_kategori')->result();
	// 	$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

	// 	$this->load->view('templates/admin_header', $data);
	// 	$this->load->view('templates/admin_sidebar', $data);
	// 	$this->load->view('admin/barang', $data);
	// 	$this->load->view('templates/admin_footer');
	// }	

// 	public function add_action($id_barang)
	// {
	// 	$data = array (
	// 		'nama_barang'=> $this->input->post('nama_barang'),
	// 		'id_kategori'=> $this->input->post('id_kategori'),
	// 		'jumlah'=> $this->input->post('jumlah'),
	// 		'merek'=> $this->input->post('merek'),
	// 		'jenis'=> $this->input->post('jenis'),
	// 		'harga' => $this->input->post('harga'),
	// 		'satuan' =>  $this->input->post('satuan')
	// 	);

	// 	$this->UserModel->insert_data($data, 'barang');
	// 	$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Data added successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
	// 		redirect('admin/barang');
    // }

	public function edit($id_barang)
	{
		$data = array (
			'id_barang' => $id_barang,
			'nama_barang'=> $this->input->post('nama_barang'),
			'jumlah'=> $this->input->post('jumlah'),
			'merek'=> $this->input->post('merek'),
			'jenis'=> $this->input->post('jenis'),
			'harga' => $this->input->post('harga'),
			'satuan' =>  $this->input->post('satuan'),
			'id_kategori'=> $this->input->post('id_kategori', true)
		);

		$this->UserModel->update_barang($data, 'barang');
		$this->session->set_flashdata('message', '<div class="alert alert-primary alert-dismissible fade show" role="alert">Data successfully changed!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/barang');
		
	} 

	public function delete($id_barang)
	{
		$where = array('id_barang' => $id_barang);

		$this->UserModel->delete($where, 'barang');
		$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Data deleted successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('admin/barang');
	}

	public function search()
	{
		$keyword = $this->input->post('keyword');
		$data['barang']=$this->UserModel->get_keyword_barang($keyword);
		$data['title'] = 'Rain Store || Barang';
		$data['sidebar'] = 'Daftar Barang';
		$data['brng'] = $this->UserModel->join('barang', 'kategori', 'barang.id_kategori=kategori.id_kategori')->result();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/barang', $data);
		$this->load->view('templates/admin_footer');
	}

	public function search_k()
	{
		$keyword = $this->input->post('keyword');
		$data['kategori']=$this->UserModel->get_keyword_kategori($keyword);
		$data['title'] = 'Rain Store || Kategori';
		$data['sidebar'] = 'Kategori Barang';
		$data['ktgr'] = $this->UserModel->get_data('kategori')->result();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/kategori', $data);
		$this->load->view('templates/admin_footer');
	}

}