<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Registration extends CI_Controller 
{
 	public function index()
	{
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
			'is_unique' => 'This email has already registered!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password1', 'Password', 'required|trim|min_length[3]|matches[password2]', [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!'
		]);
		$this->form_validation->set_rules('password2', 'Password', 'required|trim|matches[password1]');

		if ($this->form_validation->run() == false ) {
			$data['title'] = 'Registration';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/registration');
			$this->load->view('templates/auth_footer');
		} else {
			$data = [
				'name'=> htmlspecialchars($this->input->post('name', true)),
				'email'=> htmlspecialchars($this->input->post('email', true)),
				'username'=> htmlspecialchars($this->input->post('username', true)),
				'password'=> password_hash($this->input->post('password1'), PASSWORD_DEFAULT),
				'level' => 'member',
				'image' => 'default.png',
				'created_at' =>  date ("Ymd",time()),
				'is_active' => 1
			];

			$this->db->insert('user', $data);
			$this->session->set_flashdata('messagge', '<div class="alert alert-success" role="alert">Congratulation! your account has been created. Please Login</div>');
			redirect('auth');
		}
	}
}
