<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller 
{

	public function __construct()
	{
		parent:: __construct();

		if (!$this->session->userdata('email')) {
			redirect('auth');
		}
		$this->load->model('UserModel');
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['title'] = 'Rain Store || Data User';
		$data['sidebar'] = 'Data User';
        $data['user'] = $this->UserModel->get_data('user')->result();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/user', $data);
		$this->load->view('templates/admin_footer');
	}

	public function add()
	{
		$data['title'] = 'Rain Store || User';
		$this->load->view('admin/add_user', $data);
	}

	public function add_action()
	{
		$this->rules();
		
		if ($this->form_validation->run() == false) {
			$this->load->view('admin/add_user');
		} else {
			$data = array (
				'name'=> $this->input->post('name'),
				'email'=> $this->input->post('email'),
				'username'=> $this->input->post('username'),
				'password'=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'level' => $this->input->post('level'),
				'created_at' =>  date ("Ymd",time()),
				'is_active' => 1
			);

			$this->UserModel->insert_data($data, 'user');
			$this->session->set_flashdata('message', '<div class="alert alert-success alert-dismissible fade show" role="alert">Data added successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
				redirect('user');
		}
	}

	public function edit($id)
	{
		$data = array (
			'id' => $id,
			'name'=> $this->input->post('name'),
			'email'=> $this->input->post('email'),
			'username'=> $this->input->post('username'),
			'password'=> password_hash($this->input->post('password'), PASSWORD_DEFAULT),
			'level' => $this->input->post('level'),
			'created_at' =>  date ("Ymd",time()),
			'is_active' => 1,
		);

		$this->UserModel->update_data($data, 'user');
		$this->session->set_flashdata('message', '<div class="alert alert-primary alert-dismissible fade show" role="alert">Data successfully changed!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('user');
		
	} 

	public function rules()
	{
		$this->form_validation->set_rules('name', 'Name', 'required|trim');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email|is_unique[user.email]', [
			'is_unique' => 'This email has already registered!'
		]);
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('level', 'Level', 'required|trim');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|min_length[3]' , [
			'matches' => 'Password dont match!',
			'min_length' => 'Password too short!'
		]);
	}

	public function delete($id)
	{
		$where = array('id' => $id);

		$this->UserModel->delete($where, 'user');
		$this->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissible fade show" role="alert">Data deleted successfully!<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
			redirect('user');
	}

	public function search()
	{
		$keyword = $this->input->post('keyword');
		$data['user']=$this->UserModel->get_keyword($keyword);
		$data['title'] = 'Rain Store || Data User';
		$data['sidebar'] = 'Data User';
        $data['all'] = $this->UserModel->get_data('user')->result();
		$data['input'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

		$this->load->view('templates/admin_header', $data);
		$this->load->view('templates/admin_sidebar', $data);
		$this->load->view('admin/user', $data);
		$this->load->view('templates/admin_footer');
	}
}
