<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{
    public function index()
    {
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('email','Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password','Password', 'trim|required');
	
		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login';
			$this->load->view('templates/auth_header', $data);
			$this->load->view('auth/login');
			$this->load->view('templates/auth_footer');
		} else {
			// validasi success
			$this->_login();
		}
    }


	private function _login()
	{
		$this->load->library('form_validation');
		$this->load->library('session');

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$user = $this->db->get_where('user', ['email' => $email])->row_array();
		
		// jika usernya ada
		if($user) {
			// jika usernya aktif
			if($user['is_active'] == 1) {
				// cek password
				if(password_verify($password, $user['password'])) {
					$data = [
						'email' => $user['email'],
						'password' => $user['password']
					];
					$this->session->set_userdata($data);
					redirect('admin');
				}else {
					$this->session->set_flashdata('messagge', '<div class="alert alert-danger" role="alert">Wrong password!</div>');
					redirect('auth');
				}
			}else {
				$this->session->set_flashdata('messagge', '<div class="alert alert-danger" role="alert">This email has not been activated!</div>');
				redirect('auth');
			}
		} else {
			$this->session->set_flashdata('messagge', '<div class="alert alert-danger" role="alert">Email is not registered!</div>');
			redirect('auth');
		}
	}

	/*public function protected()
	{
		$this->load->library('session');

		if ($this->session->userdata('email') == '') {
			$this->session->set_flashdata('messagge', '<div class="alert alert-danger" role="alert">you are not logged in</div>');
			redirect('auth');
		}
	}*/

	public function logout()
	{
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('level');

		$this->session->set_flashdata('messagge', '<div class="alert alert-success" role="alert">You have been logged out!</div>');
		redirect('auth');
	}
}