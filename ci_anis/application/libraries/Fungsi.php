<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Fungsi 
{
    protected $ci;

    function __construct()
    {
        $this->ci = &get_instance();
    }

    function user_login()
    {
        $this->ci->load->model('UserModel');
        $id= $this->ci->session->userdata('id');
        $user_data = $this->ci->UserModel->get($id)->row();
        return $user_data;
    }
}
