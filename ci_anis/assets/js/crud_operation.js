$(document).ready(function () {
    listUsers();
    $('#listUserTable').dataTable({
       "bPaginate": false,
       "bInfo": false,
       "bFilter": false,
       "bLengthChange": false,
       "pageLength": 5
    });
 // list all user in datatable
    function listUsers() {
       $.ajax({
          type: 'ajax',
          url: 'user/tampilkanData',
          async: false,
          dataType: 'json',
          success: function (data) {
             var html = '';
             var i;
             var no = 1;
          for (i = 0; i < data.length; i++) {
              html += '<tr id="' + data[i].id + '">' +
                '<td>' + no++ + '</td>' +
				'<td>' + data[i].name + '</td>' +
                '<td>' + data[i].username + '</td>' +
                '<td>' + data[i].email + '</td>' +
				'<td>' + data[i].level + '</td>' +
                '<td style="text-align:right;">' +
                '<a href="javascript:void(0);" class="btn btn-info btn-sm editRecord" data-id="' + data[i].id + '" data-name="' + data[i].name + '" data-username="' + data[i].username + '"data-email="' + data[i].email + '" data-level="' + data[i].level +  '">Edit</a>' + ' ' +
                '<a href="javascript:void(0);" class="btn btn-danger btn-sm deleteRecord" data-id="' + data[i].id + '">Delete</a>' +
                '</td>' +
                '</tr>';
    }
    $('#listUser').html(html);
  }
 });

}

// save new user record
$('#saveUserForm').submit('click', function () {
	var Username = $('#name').val();
    var UserUsername = $('#username').val();
    var UserEmail = $('#email').val();
	var UserLevel = $('#level').val();
    var UserPassword = $('#password').val();
    $.ajax({
        type: "POST",
        url: "user/simpanData",
        dataType: "JSON",
        data: { name: UserName, username: UserUsername, email: UserEmail, level: UserLevel, password: UserPassword },
        success: function (data) {
           $('#name').val("");
		   $('#username').val("");
           $('#email').val("");
		   $('#level').val("");
           $('#password').val("");
           $('#addUserModal').modal('hide');
           listUsers();
     }
   });
   return false;
});

// show edit modal form with user data
$('#listUser').on('click', '.editRecord', function () {
    $('#editUserModal').modal('show');
    $("#userId").val($(this).data('id'));
	$("#nameEdit").val($(this).data('name'));
    $("#usernameEdit").val($(this).data('username'));
    $("#emailEdit").val($(this).data('email'));
	$("#levelEdit").val($(this).data('level'));
});
// save edit record
$('#editUserForm').on('submit', function () {
    var id = $('#userId').val();
	var name = $('#nameEdit').val();
    var username = $('#usernameEdit').val();
    var email = $('#emailEdit').val();
	var level = $('#levelEdit').val();
    $.ajax({
       type: "POST",
       url: "user/update",
       dataType: "JSON",
       data: { name: UserName, username: UserUsername, email: UserEmail, level: UserLevel, password: UserPassword },
        success: function (data) {
           $('#name').val("");
		   $('#username').val("");
           $('#email').val("");
		   $('#level').val("");
           $('#password').val("");
           $('#addUserModal').modal('hide');
           listUsers();
    }
  });
  return false;
});

// show delete modal
$('#listUser').on('click', '.deleteRecord', function () {
    var UserId = $(this).data('id');
    $('#deleteUserModal').modal('show');
    $('#deleteUserId').val(UserId);
});
// delete user record
$('#deleteUserForm').on('submit', function () {
    var UserId = $('#deleteUserId').val();
    $.ajax({
       type: "POST",
       url: "user/hapus",
       dataType: "JSON",
       data: { id: UserId },
       success: function (data) {
       $("#" + UserId).remove();
       $('#deleteUserId').val("");
       $('#deleteUserModal').modal('hide');
       listUsers();
    }
  });
  return false;
 });
});

