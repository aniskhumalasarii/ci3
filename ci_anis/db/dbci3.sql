-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: db_ci3
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(255) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `jumlah` int(11) NOT NULL,
  `merek` varchar(255) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `harga` varchar(115) NOT NULL,
  `satuan` varchar(255) NOT NULL,
  PRIMARY KEY (`id_barang`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (2,'Mouse',2,15,'Robot','wireless','86.000','Unit'),(4,'Pulpen',5,50,'Pilot','Hitam','30.000','Pack'),(5,'Laptop',2,100,'Lenovo','Ideapad slim 3','7.500.000','unit'),(8,'handphone',1,14,'Vivo','y91c','1.500.000','unit'),(9,'Kalung',3,1,'Cartier','Magnitude, Thiea','322.000.000','Perset'),(23,'earphone',2,10,'Fantech SCAR EG2','berkabel','149.000','unit');
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'Smartfren'),(2,'Perangkat Elektronik'),(3,'Perhiasan'),(5,'Alat Tulis'),(6,'Aksesoris');
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(125) NOT NULL AUTO_INCREMENT,
  `name` varchar(125) NOT NULL,
  `email` varchar(125) NOT NULL,
  `username` varchar(125) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('admin','member') NOT NULL,
  `image` varchar(225) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `is_active` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (88,'Nezzara Zialiyana','nezza@gmail.com','neza','$2y$10$VpTdNdjF5wM8SrRGdGUd3ecrlH0aA/BZKs8sPEex8xT1fTilBy8WG','admin','default.png','2022-05-19 17:00:00',1),(95,'lia','lia@gmail.com','lia','$2y$10$ZMn/QdbBjiFTrvuDofwmNeitAy2uEh8zM.OiTjYolR5SWjJ4mELlW','admin','default.png','2022-05-16 17:00:00',1),(96,'Alfanzo Reagan','Alfa@gmail.com','ega','$2y$10$Ow4BpdlXiBa3/Sh/unROne9btLh5a9TuV4iHTOfBPJsbnObA9nrcK','member','default.png','2022-04-20 17:00:00',1),(97,'Cecilia Anatasya','anatasya@gmail.com','cilia','$2y$10$Lbj3gb/83HJIpvJJVdfb/uvtQ5MJ8iJQGYfekwmH2GBOXATjX9Zg.','admin','default.png','2022-05-16 17:00:00',1),(98,'Alice Lizzyanaa','lizzy@gmail.com','lizzy','$2y$10$7FKB3DAHLojckRF4KqPc3eMgvRe/8sTPmLMX2t3oHuV8ly08tj3GK','member','lizzy.jpeg','2022-05-12 17:00:00',1),(99,'Anis khumalasari','aniss@gmail.com','anisa','$2y$10$ixr9pAHzN.1.3FWlsxbqEuTeoZX7DgDfrtCBlJu1eDBIzsnAtMMYO','admin','default.png','2022-07-26 17:00:00',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-28  9:42:21
